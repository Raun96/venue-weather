//
//  ServiceManager.swift
//  VenueWeather
//
//  Created by Raunak on 26/9/20.
//

import Foundation

class ServiceManager {
    
    private let dataApi = "http://dnu5embx6omws.cloudfront.net/venues/weather.json"
    
    /// Network call to the API using URLSession
    func handleNetworkcall(completionHandler: @escaping (_ status: Bool, _ venues: [Venue]?)  -> ()) {
        let url = URL(string: dataApi)!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                
            let decoder = JSONDecoder()
            guard let data = data else {
                completionHandler(false, nil)
                return
            }
            do {
                let venues = try decoder.decode(Venues.self, from: data)
                completionHandler(true, venues.data)
                } catch {
                    print(error)
                }
        }
        task.resume()
    }
}
