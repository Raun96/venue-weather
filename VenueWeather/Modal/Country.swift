//
//  Country.swift
//  VenueWeather
//
//  Created by Raunak on 26/9/20.
//

import Foundation

struct Country: Codable, Hashable {
    var name: String
    var countryID: String
    
    enum CodingKeys: String, CodingKey {
        case name = "_name"
        case countryID = "_countryID"
    }
}
