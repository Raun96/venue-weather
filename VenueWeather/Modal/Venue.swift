//
//  Venue.swift
//  VenueWeather
//
//  Created by Raunak on 26/9/20.
//

import Foundation

struct Venue: Codable, Hashable {
    
    var venueID: String
    var name: String
    var country : Country
    
    var weatherCondition: String?
    var weatherConditionIcon: String?
    var wind: String?
    var humidity: String?
    var temperature: String?
    var temperatureFeelsLike: String?
    var lastUpdated: Int?
    
    private enum CodingKeys: String, CodingKey {
        case venueID = "_venueID"
        case name = "_name"
        case country = "_country"
        case weatherCondition = "_weatherCondition"
        case weatherConditionIcon = "_weatherConditionIcon"
        case wind = "_weatherWind"
        case humidity = "_weatherHumidity"
        case temperature = "_weatherTemp"
        case temperatureFeelsLike = "_weatherFeelsLike"
        case lastUpdated = "_weatherLastUpdated"
    }
}
