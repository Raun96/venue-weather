//
//  venuesData.swift
//  VenueWeather
//
//  Created by Raunak on 26/9/20.
//

import Foundation

struct Venues: Codable {
    
    var data : [Venue]
    
}
