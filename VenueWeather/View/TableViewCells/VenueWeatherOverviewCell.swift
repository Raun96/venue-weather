//
//  VenueWeatherOverviewCell.swift
//  VenueWeather
//
//  Created by Raunak on 26/9/20.
//

import UIKit

class VenueWeatherOverviewCell: UITableViewCell {
    
    @IBOutlet var venueNameLabel: UILabel!
    @IBOutlet var venueWeatherLabel: UILabel!
    @IBOutlet var venueWeatherImageView: UIImageView!
    @IBOutlet var venueTemperatureLabel: UILabel!
    
    func configureCell(venue: Venue) {
        venueNameLabel.text = venue.name
        venueWeatherLabel.text = venue.weatherCondition ?? "N/A"
        venueTemperatureLabel.text = venue.temperature != nil ? "\(venue.temperature ?? "")°" : "-"
        venueWeatherImageView.image = WeatherType.getWeatherIconName(for: venue.weatherCondition)
    }

    /*
     Cases handled by the WeatherType enum - any weather outside these cases results in an weather image being empty
     ["Scattered Clouds", "Light Rain", "Mist", "Light Rain Showers", "Thunderstorm", "Overcast", "Smoke", "Clear", "Fog", "Rain", "Snow", "Mostly Cloudy", "Patches of Fog", "Shallow Fog", "Partly Cloudy", "Haze"]
    */
   private enum WeatherType: String, CaseIterable {
    case scatteredClouds = "Scattered Clouds"
    case lightRain = "Light Rain"
    case mist = "Mist"
    case lightRainShowers = "Light Rain Showers"
    case thunderstorm = "Thunderstorm"
    case overcast = "Overcast"
    case smoke = "Smoke"
    case clear = "Clear"
    case fog = "Fog"
    case rain = "Rain"
    case snow = "Snow"
    case mostlyCloudy = "Mostly Cloudy"
    case patchesOfFog = "Patches Of Fog"
    case shallowFog = "Shallow Fog"
    case partlyCloudy = "Partly Cloudy"
    case haze = "Haze"

    static func getWeatherType(weather: String) -> WeatherType? {
        return WeatherType.allCases.filter { weather.elementsEqual($0.rawValue) }.first
    }
    
    static func getWeatherIconName(for weather: String?) -> UIImage? {
        
        guard let weatherType = WeatherType.getWeatherType(weather: weather ?? "") else {
            return nil
        }
        switch weatherType {
        case .clear:
            return UIImage(systemName: "sun.max")
        case .fog, .shallowFog, .patchesOfFog:
            return UIImage(systemName: "cloud.fog.fill")
        case .lightRain, .lightRainShowers:
            return UIImage(systemName: "cloud.rain.fill")
        case .mist:
            return UIImage(systemName: "sun.dust")
        case .mostlyCloudy:
            return UIImage(systemName: "cloud.fill")
        case .overcast:
            return UIImage(systemName: "smoke.fill")
        case .rain:
            return UIImage(systemName: "cloud.heavyrain.fill")
        case .scatteredClouds, .partlyCloudy:
            return UIImage(systemName: "cloud")
        case .smoke:
            return UIImage(systemName: "smoke")
        case .snow:
            return UIImage(systemName: "snow")
        case .thunderstorm:
            return UIImage(systemName: "cloud.bolt.rain.fill")
        case .haze:
            return UIImage(systemName: "sun.haze")
        }
    }
        
    }

}
