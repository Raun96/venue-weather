//
//  VenueWeatherDetailCell.swift
//  VenueWeather
//
//  Created by Raunak on 27/9/20.
//

import UIKit

class VenueWeatherDetailCell: UITableViewCell {

    @IBOutlet var humidityLabel: UILabel!
    @IBOutlet var windLabel: UILabel!
    @IBOutlet var feelsLikeLabel: UILabel!
    
    func configureCell(venue: Venue) {
        feelsLikeLabel.text = venue.temperatureFeelsLike != nil ? "\(venue.temperatureFeelsLike ?? "")°" : "-"
        
        if let humidity = venue.humidity {
            humidityLabel.text = humidity.components(separatedBy: ": ").last
        } else {
            humidityLabel.text = "-"
        }
       
        if let wind = venue.wind {
            windLabel.text = wind.components(separatedBy: ": ").last
        } else {
            windLabel.text = "-"
        }
    }
    
}
