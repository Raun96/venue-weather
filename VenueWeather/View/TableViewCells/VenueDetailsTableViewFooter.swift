//
//  VenueDetailsTableViewFooter.swift
//  VenueWeather
//
//  Created by Raunak on 27/9/20.
//

import UIKit

class VenueDetailsTableViewFooter: UITableViewHeaderFooterView {

    var title = UILabel()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        configureFooterView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureFooterView() {
        title.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(title)
        
        title.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        title.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        title.heightAnchor.constraint(equalToConstant: 30).isActive = true
        title.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        
        
        title.font = UIFont(name: "Avenir", size: 18)
        title.textColor = .systemGray
        title.textAlignment = .center
        self.contentView.backgroundColor = .systemBackground
    }
    
    func configureLastUpdated(timeStamp: Int?) {
        
        guard let timeStamp = timeStamp else {
            title.text = "Last Updated: N/A"
            return
        }
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        
        let dateTime = dateFormatter.string(from: date)
        title.text = "Last Updated: \(dateTime)"
        
    }
}
