//
//  FilterCountriesCell.swift
//  VenueWeather
//
//  Created by Raunak on 27/9/20.
//

import UIKit

class FilterCountriesCell: UITableViewCell {

    @IBOutlet var countrylabel: UILabel!
    
    func configureCell(country: Country) {
        countrylabel.text = country.name
    }

}
