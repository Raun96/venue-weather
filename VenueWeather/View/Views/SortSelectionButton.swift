//
//  SortSelectionButton.swift
//  VenueWeather
//
//  Created by Raunak on 26/9/20.
//

import UIKit

enum SortType: String, CaseIterable {
    case alphabatically = "A-Z"
    case temperature = "Temperature"
    case updates = "Last Updated"
    
    static func typeFor(buttonTitle: String) -> SortType? {
        return SortType.allCases.filter {$0.rawValue == buttonTitle}.first
    }
}

class SortSelectionButton: UIButton {
    
    init(sortType: SortType) {
        super.init(frame: .zero)
        setTitle(sortType.rawValue, for: .normal)
        setTitleColor(.black, for: .normal)
        titleLabel?.font = UIFont(name: "Avenir", size: 16)
        titleLabel?.textAlignment = .center
        setupButton()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupButton()
    }
    
    var selectionView: UIView!
    
    private func setupButton() {
        self.setTitleColor(UIColor.label, for: .normal)
        selectionView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        self.addSubview(selectionView)
        setupSelectionView()
    }
    
    private func setupSelectionView() {
        selectionView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            selectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            selectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 4),
            selectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -4),
            selectionView.heightAnchor.constraint(equalToConstant: 4)
        ]
        
        NSLayoutConstraint.activate(constraints)
        selectionView.layer.cornerRadius = 2
    }
    
    func setButtonSelection(isSelected: Bool) {
        self.titleLabel?.font = isSelected ? UIFont(name: "Avenir-Heavy", size: 17): UIFont(name: "Avenir", size: 16)
        selectionView.backgroundColor = isSelected ? UIColor.systemBlue : UIColor.systemBackground
    }
    
}
