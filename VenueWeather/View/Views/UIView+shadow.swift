//
//  UIView+shadow.swift
//  VenueWeather
//
//  Created by Raunak on 27/9/20.
//

import UIKit

extension UIView {
    func setBottomShadow() {
        layer.masksToBounds = false
        layer.shadowRadius = 2
        layer.shadowOpacity = 0.45
        layer.shadowColor = UIColor.systemGray.cgColor
        layer.shadowOffset = .zero
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    }
}
