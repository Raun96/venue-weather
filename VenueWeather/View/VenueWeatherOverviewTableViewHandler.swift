//
//  VenueWeatherOverviewTableviewHandler.swift
//  VenueWeather
//
//  Created by Raunak on 26/9/20.
//

import UIKit

protocol WeatherOverviewTableViewHandlerDelegate: class {
    func weatherOverviewTableViewHandler( didSelectVenue venue: Venue)
}

class VenueWeatherOverviewTableViewHandler: NSObject {
    
    fileprivate enum Sections {
        case main
    }
    
    private var dataSource : UITableViewDiffableDataSource<Sections, Venue>!
    private let venueOverviewCellNib = UINib(nibName: "VenueWeatherOverviewCell", bundle: .main)
    
    weak var delegate: WeatherOverviewTableViewHandlerDelegate?
    
    func configureTableView(tableView: UITableView, venues: [Venue]) {
        
        tableView.register(venueOverviewCellNib, forCellReuseIdentifier: "venueOverviewCell")
        tableView.delegate = self
        configureDataSource(tableView: tableView)
        createSnapshot(from: venues)
    }
    
    private func configureDataSource(tableView: UITableView) {
        dataSource = UITableViewDiffableDataSource<Sections, Venue>(tableView: tableView, cellProvider: { (tableView, indexPath, venue) -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: "venueOverviewCell", for: indexPath) as! VenueWeatherOverviewCell
            cell.configureCell(venue: venue)
            return cell
        })
    }
    
     func createSnapshot(from venues: [Venue]) {
        var snapshot = NSDiffableDataSourceSnapshot<Sections, Venue>()
        
        snapshot.appendSections([.main])
        snapshot.appendItems(venues)
        dataSource.apply(snapshot, animatingDifferences: true)
    }  
}

extension VenueWeatherOverviewTableViewHandler: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let venue = dataSource.itemIdentifier(for: indexPath) else { return }
        delegate?.weatherOverviewTableViewHandler(didSelectVenue: venue)
    }
    
}
