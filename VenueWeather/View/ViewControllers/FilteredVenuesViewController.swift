//
//  FilteredVenuesViewController.swift
//  VenueWeather
//
//  Created by Raunak on 27/9/20.
//

import UIKit

class FilteredVenuesViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    static let nibName = "FilteredVenuesViewController"
    
    private let tableViewHandler = VenueWeatherOverviewTableViewHandler()
    private var viewModal: VenueWeatherOverviewViewModal!
    
    init(venues: [Venue]) {
        super.init(nibName: FilteredVenuesViewController.nibName, bundle: .main)
        viewModal = VenueWeatherOverviewViewModal(venues: venues)
        self.title = venues.first?.country.name
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewHandler.configureTableView(tableView: tableView, venues: viewModal.venues)
        tableViewHandler.delegate = self
        tableView.tableFooterView = UIView(frame: .zero)
    }

}
extension FilteredVenuesViewController: WeatherOverviewTableViewHandlerDelegate {
    func weatherOverviewTableViewHandler(didSelectVenue venue: Venue) {
       let venueDetailViewController = VenueWeatherDetailViewController(venue: venue)
       self.navigationController?.pushViewController(venueDetailViewController, animated: true)
    }
    
    
}
