//
//  FilterCountriesViewController.swift
//  VenueWeather
//
//  Created by Raunak on 27/9/20.
//

import UIKit

class FilterCountriesViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    fileprivate enum Sections {
           case main
       }
    
    static let nibName = "FilterCountriesViewController"
    var viewModal: FilterCountriesViewModal!
    private var dataSource : UITableViewDiffableDataSource<Sections, Country>!
    private let filterCountriesCellNib = UINib(nibName: "FilterCountriesCell", bundle: .main)
    
    init(venuesBycountries: Dictionary<Country,[Venue]>) {
        super.init(nibName: FilterCountriesViewController.nibName, bundle: .main)
        viewModal = FilterCountriesViewModal(venuesByCountries: venuesBycountries)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(filterCountriesCellNib, forCellReuseIdentifier: "filterCountriesCell")
        configureDataSource()
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.delegate = self
        setupNavigationBar()
      
    }
    
    private func setupNavigationBar() {
        self.navigationController?.navigationBar.topItem?.title = "Filter"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

    private func configureDataSource() {
        dataSource = UITableViewDiffableDataSource<Sections, Country>(tableView: tableView, cellProvider: { (tableView, indexPath, country) -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: "filterCountriesCell", for: indexPath) as! FilterCountriesCell
            cell.configureCell(country: country)
            return cell
        })
        createSnapshot(from: viewModal.countries)
    }
    
     private func createSnapshot(from countries: [Country]) {
        var snapshot = NSDiffableDataSourceSnapshot<Sections, Country>()
        
        snapshot.appendSections([.main])
        snapshot.appendItems(countries)
        dataSource.apply(snapshot, animatingDifferences: true)
    }

}

extension FilterCountriesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let country = dataSource.itemIdentifier(for: indexPath) else {
            return
        }
        let filteredVenuesVC = FilteredVenuesViewController(venues: viewModal.venuesForCountry(country: country))
        self.navigationController?.pushViewController(filteredVenuesVC, animated: true)
    }
}
