//
//  VenueWeatherOverviewViewController.swift
//  VenueWeather
//
//  Created by Raunak on 26/9/20.
//

import UIKit

class VenueWeatherOverviewViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var sortSelectionStack: UIStackView!
    @IBOutlet var selectionView: UIView!
    @IBOutlet var filterBarButton: UIBarButtonItem!
    
    private let refreshController = UIRefreshControl()
    private let tableViewHandler = VenueWeatherOverviewTableViewHandler()
    private var viewModal: VenueWeatherOverviewViewModal!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        configureButtons()
        configureViewController()
        refreshController.addTarget(self, action: #selector(refreshWeatherData), for: .valueChanged)
    }
    
    private func configureNavigationBar() {
        self.title = "Weather"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        filterBarButton?.target = self
        filterBarButton?.action = #selector(didTapFilter)
    }
    
    private func configureViewController() {
        let serviceManager = ServiceManager()
        viewModal = VenueWeatherOverviewViewModal(serviceManager: serviceManager)
        viewModal.delegate = self
        
        tableViewHandler.configureTableView(tableView: tableView, venues: [Venue]())
        tableViewHandler.delegate = self
        tableView.refreshControl = refreshController
        
    }
    
    /// Configures button for sorting  selection
    private func configureButtons() {
        let buttons = [SortSelectionButton(sortType: .alphabatically),
                       SortSelectionButton(sortType: .temperature),
                       SortSelectionButton(sortType: .updates)]
        
        buttons.forEach { button in
            sortSelectionStack.addArrangedSubview(button)
            let constraints = [
                NSLayoutConstraint(item: button, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: sortSelectionStack.frame.height)]
            
            button.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
            guard let title = button.titleLabel?.text else {return}
            if SortType.typeFor(buttonTitle: title) == SortType.alphabatically {
                button.setButtonSelection(isSelected: true)
            }
            sortSelectionStack.addConstraints(constraints)
        }
        sortSelectionStack.distribution = .fillEqually
        selectionView.setBottomShadow()
    }
    
    @objc private func buttonTapped(_ button: SortSelectionButton) {
        guard let title = button.titleLabel?.text, let sortType = SortType.typeFor(buttonTitle: title) else {
            return
        }
        for button in sortSelectionStack.arrangedSubviews as! [SortSelectionButton] {
            button.setButtonSelection(isSelected: false)
        }
        
        button.setButtonSelection(isSelected: true)
        viewModal.sortVenues(sortType: sortType)
    }
    
    @objc private func didTapFilter() {
        let filterCountriesVC = FilterCountriesViewController(venuesBycountries: viewModal.getVenuesByCountries())
        let filterCountriesNavigationController = UINavigationController(rootViewController: filterCountriesVC)
        self.present(filterCountriesNavigationController, animated: true, completion: nil)
    }
    
    @objc private func refreshWeatherData() {
        viewModal.getVenueData()
    }
    
}

extension VenueWeatherOverviewViewController: VenueWeatherDelegate {
    func didFinishLoadingData(status: Bool) {
        
        guard status else {
            let alert = UIAlertController(title: "Couldn't load Data", message: "Oops! unfortunately we can't update weather details for venues.\nPlease try again later", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
            return
        }
        guard let venues = viewModal.venues else {
            return
        }
        DispatchQueue.main.sync {
            refreshController.endRefreshing()
            self.tableViewHandler.createSnapshot(from: venues)
        }
    }
    
    func didFinishSortingData() {
        guard let venues = viewModal.venues else {
            return
        }
        DispatchQueue.main.async {
            self.tableViewHandler.createSnapshot(from: venues)
        }
        
    }
}

extension VenueWeatherOverviewViewController: WeatherOverviewTableViewHandlerDelegate {
    func weatherOverviewTableViewHandler(didSelectVenue venue: Venue) {
        let venueDetailViewController = VenueWeatherDetailViewController(venue: venue)
        self.navigationController?.pushViewController(venueDetailViewController, animated: true)
    }
}

