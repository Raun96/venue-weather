//
//  VenueWeatherDetailViewController.swift
//  VenueWeather
//
//  Created by Raunak on 27/9/20.
//

import UIKit

class VenueWeatherDetailViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var venue: Venue!
    let overviewCellNib = UINib(nibName: "VenueWeatherOverviewCell", bundle: nil)
    let detailsCellNib = UINib(nibName: "VenueWeatherDetailCell", bundle: nil)
    static let nibName = "VenueWeatherDetailViewController"
    
    init(venue: Venue) {
        super.init(nibName: VenueWeatherDetailViewController.nibName, bundle: .main)
        self.venue = venue
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Weather Details"
        tableView.dataSource = self
        tableView.delegate = self
        configureTableView()
    }
    
    private func configureTableView() {
        tableView?.register(overviewCellNib, forCellReuseIdentifier: "venueOverviewCell")
        tableView?.register(detailsCellNib, forCellReuseIdentifier: "venueWeatherDetailCell")
        tableView?.register(VenueDetailsTableViewFooter.self, forHeaderFooterViewReuseIdentifier: "VenueDetailsTableViewFooter")
        tableView.isUserInteractionEnabled = false
    }
}

extension VenueWeatherDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let overviewCell = tableView.dequeueReusableCell(withIdentifier: "venueOverviewCell") as! VenueWeatherOverviewCell
            overviewCell.configureCell(venue: venue)
            return overviewCell
        } else if indexPath.row == 1{
            let detailsCell = tableView.dequeueReusableCell(withIdentifier: "venueWeatherDetailCell") as! VenueWeatherDetailCell
            detailsCell.configureCell(venue: venue)
            return detailsCell
        } else {
            return UITableViewCell()
        }
    }
    
    // sets height for 2 rows explicitely to display the weather details
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 100 : 80
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "VenueDetailsTableViewFooter") as? VenueDetailsTableViewFooter else {
            return nil
        }
        view.configureLastUpdated(timeStamp: venue.lastUpdated)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30
    }
    
}
