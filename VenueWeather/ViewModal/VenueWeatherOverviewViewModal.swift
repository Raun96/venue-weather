//
//  VenueWeatherOverviewViewModal.swift
//  VenueWeather
//
//  Created by Raunak on 26/9/20.
//

import Foundation

protocol VenueWeatherDelegate {
    func didFinishLoadingData(status: Bool)
    func didFinishSortingData()
}

// encapsualates the logic behind displaying the weather for all locations
class VenueWeatherOverviewViewModal {
    
    
    var venues: [Venue]!
    private let serviceManager: ServiceManager?
    
    private var lastSelectedSortType : SortType?
    var delegate: VenueWeatherDelegate?
    
    init(serviceManager: ServiceManager) {
        self.serviceManager = serviceManager
        venues = [Venue]()
        getVenueData()
    }
    
    init(venues: [Venue]) {
        self.venues = venues
        serviceManager = nil
        sortVenuesAlphabatically()
        lastSelectedSortType = SortType.alphabatically
    }
    
    func getVenueData() {
        serviceManager?.handleNetworkcall { [weak self] (status, venues) in
            switch status {
            case true:
                guard let venues = venues else {return}
                self?.venues = venues
                self?.sortVenues(sortType: self?.lastSelectedSortType ?? SortType.alphabatically)
                self?.delegate?.didFinishLoadingData(status: true)
            case false:
                self?.delegate?.didFinishLoadingData(status: false)
            }
        }
    }
    
    func sortVenues(sortType: SortType) {
        switch sortType {
        case .alphabatically:
            sortVenuesAlphabatically()
        case .temperature:
            sortVenuesByTemperature()
        case .updates:
            sortVenuesByLastUpdated()
        }
        
        lastSelectedSortType = sortType
        delegate?.didFinishSortingData()
    }
    
    private func sortVenuesAlphabatically() {
        venues = venues.sorted { $0.name < $1.name}
    }
    
    
    private func sortVenuesByTemperature() {
        venues = venues.sorted { Int($0.temperature ?? "0")! > Int($1.temperature ?? "0")!  }
    }
    
    private func sortVenuesByLastUpdated() {
        venues = venues.sorted { $0.lastUpdated ?? 0 > $1.lastUpdated ?? 0 }
    }
    
    /// Sorts all Countries for venues into a dictionary
    /// - Returns: Dictionary containing all countries as keys and venues belonging to a country
    func getVenuesByCountries() -> Dictionary<Country,[Venue]> {
        
        var countriesDictionary = Dictionary<Country,[Venue]>()
        
        var countriesSet = Set<Country>()
        venues.forEach { (venue) in
            countriesSet.insert(venue.country)
        }
       
        countriesSet.forEach { (country) in
            let venuesForCountry = venues.filter { country.name.elementsEqual($0.country.name)}
            
            countriesDictionary[country] = venuesForCountry
        }
        return countriesDictionary
    }
}
