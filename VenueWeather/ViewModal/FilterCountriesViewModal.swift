//
//  FilterCountriesViewModal.swift
//  VenueWeather
//
//  Created by Raunak on 27/9/20.
//

import Foundation

class FilterCountriesViewModal {
    
    var venuesByCountries : Dictionary<Country,[Venue]>!
    var countries: [Country]!
    
    init(venuesByCountries: Dictionary<Country,[Venue]>) {
        self.venuesByCountries = venuesByCountries
        sortVenuesAlphabetically()
    }
    
    private func sortVenuesAlphabetically() {
        let unsortedCountries = Array(venuesByCountries.keys)
        countries = unsortedCountries.sorted(by: { $0.name < $1.name })
    }
    
    func venuesForCountry(country: Country) -> [Venue] {
        guard let venues = venuesByCountries[country] else {
            return [Venue]()
        }
        return venues
        
    }
    
}
