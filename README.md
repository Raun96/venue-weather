# Venue Weather

    This application allows users to browse through a list of sporting venues, and know weather details about them.
    
    The venues are sortable alphabatically, temperature (descending order) and last updated (descending order). Users can also filter venues based on countries.
    
    The project developed using xcode 14.0 with minimum delpoyment target of iOS 13.

# Architecture

    The app follows the MVVM architecture.

    With emphasis on modularity and code reusabilty of certain UI components using xibs and ViewModals. 

    UI for the app is developed using storyboard, xibs and in code. 

# Highlights of the project
 
- #### Diffable Datasource
        The app uses diffable datasource for its tableViews, to handle updates to the tableViews. As UIKit handles the heavy lifting such as finding changes in the data and animating changes, and also saves us the hassle of keeping the data in sync with the tableView sections and indexes.

- #### Dark Mode
        The app also supports dark mode by using system colors throughout that shift between the two modes.

- #### SFSymbols 
        The app also displays weather information using visual elements in the form of symbols for different conditions.


# What's Next

        - Add search bar to search for venues
        
